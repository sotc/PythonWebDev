# Hangman Game

word_list = ["recitation", "valuation", "testimony"]

# Step 1

# TODO-1: Randomly choose a word from word_list and assign it to a variable chosen_word.


# TODO-2: Ask user to guess a letter and assign their answer to a variable called guess. Make guess lowercase.


# TODO-3: Check if the letter the user guessed is one of the letters in the chosen_word.
